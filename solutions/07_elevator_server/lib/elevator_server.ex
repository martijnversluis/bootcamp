defmodule ElevatorServer do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      Plug.Adapters.Cowboy.child_spec(:http, ElevatorServer.Router, [], port: 8080, acceptors: 10),
      worker(Elevator, [10])
    ]

    IO.puts "Started the application"

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
