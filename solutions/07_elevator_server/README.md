# ElevatorServer

- Register the Elevator GenServer under the name Elevator

- Start the Elevator GenServer in your supervisor tree

- Create the OTP application:

  - Add deps for Plug and Cowboy
  - Create the application supervisor with the Cowboy listener
  - Create the Plug router (ElevatorServer.Router)

- Define API endpoints (in the Router)

  - GET /
    Status endpoint; should return JSON object with current floor, et cetera

  - POST /goto/:floor
    Should set the elevator in motion. Can return HTTP 204 status.

- Implement endpoints; call the appropriate genserver call and
  serialize the response to JSON.

- Make elevator simulation "tick" automatically, using `Process.send_after/2` in another "clock" genserver
