# Distributed ElevatorServer


  def start_link(num_floors, start_floor \\ 1) do
    GenServer.start_link(Elevator, {num_floors, start_floor}, name: Elevator)
  end

- iex -S mix
  > starts the server

- Ensure Elevator GenServer under the name Elevator
  you should be able to call it like this: Elevator.tick(Elevator)

- Set up Elixir distribution
  > # iex --name a@127.0.0.1 --cookie hello -S mix
  > # iex --name b@127.0.0.1 --cookie hello -S mix

- node() B needs to listen on different port!

- Ensure the elevator genserver ONLY is started when
  node() == :"a@127.0.0.1"

- From node B, in the router, use :rpc.call to perform the elevator functions
  > inside the plug router; or move this code somewhere else
