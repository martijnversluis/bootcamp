defmodule Oops do
  @moduledoc """
  Documentation for Oops.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Oops.hello
      :world

  """
  @spec hello() :: String.t
  def hello do
    :world
  end

  @spec hello_back( String.t ) :: String.t
  def hello_back(name) do
    IO.puts "Hello #{name}"
  end
end
