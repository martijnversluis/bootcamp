// More info https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({
  history: true,
  allottedTime: 3 * 60 * 1000,
  progressBarHeight: 3,
  progress: true,
  dependencies: [
    { src: 'plugin/markdown/marked.js' },
    { src: 'plugin/markdown/markdown.js' },
    { src: 'plugin/notes/notes.js', async: true },
    { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
  ]
});
